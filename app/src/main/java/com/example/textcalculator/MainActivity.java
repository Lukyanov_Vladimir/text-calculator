package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button sumBtn, subtractionBtn, divisionBtn, multiplicationBtn, powBtn;

    EditText fieldFirstTerm, fieldSecondTerm;

    Intent intent;

    double firstTerm, secondTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sumBtn = findViewById(R.id.sumBtn);
        subtractionBtn = findViewById(R.id.subtractionBtn);
        divisionBtn = findViewById(R.id.divisionBtn);
        multiplicationBtn = findViewById(R.id.multiplicationBtn);
        powBtn = findViewById(R.id.powBtn);

        sumBtn.setOnClickListener(this);
        subtractionBtn.setOnClickListener(this);
        divisionBtn.setOnClickListener(this);
        multiplicationBtn.setOnClickListener(this);
        powBtn.setOnClickListener(this);

        fieldFirstTerm = findViewById(R.id.FieldFirstTerm);
        fieldSecondTerm = findViewById(R.id.fieldSecondTerm);
    }

    @Override
    public void onClick(View v) {
        double result = 0;
        String mathSymbol = "";
        firstTerm = Double.parseDouble(fieldFirstTerm.getText().toString());
        secondTerm = Double.parseDouble(fieldSecondTerm.getText().toString());

        switch (v.getId()) {
            case R.id.sumBtn:
                result = MathOperations.addition(firstTerm, secondTerm);
                mathSymbol = " + ";
                break;

            case R.id.subtractionBtn:
                result = MathOperations.subtraction(firstTerm, secondTerm);
                mathSymbol = " - ";
                break;

            case R.id.divisionBtn:
                result = MathOperations.division(firstTerm, secondTerm);
                mathSymbol = " / ";
                break;

            case R.id.multiplicationBtn:
                result = MathOperations.multiply(firstTerm, secondTerm);
                mathSymbol = " * ";
                break;

            case R.id.powBtn:
                result = MathOperations.pow(firstTerm, secondTerm);
                mathSymbol = "^";
                break;
        }

        intent = new Intent(this, ResultActivity.class);
        intent.putExtra("result", result);
        intent.putExtra("firstTerm", firstTerm);
        intent.putExtra("secondTerm", secondTerm);
        intent.putExtra("mathSymbol", mathSymbol);
        startActivity(intent);
    }
}
