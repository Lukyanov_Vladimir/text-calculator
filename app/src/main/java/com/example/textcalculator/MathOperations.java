package com.example.textcalculator;

/**
 * Класс MathOperations создержит методы: умножение, деление, сложение, вычитание, возведение в степень, выделение остатка
 *
 * @author Лукьянов В. А. 17ИТ18
 */
class MathOperations {
    /**
     * Метод выполняет умножение двух чисел
     *
     * @param numberOne - первое число
     * @param numberTwo - второе число
     * @return - произведение
     */
    static double multiply(double numberOne, double numberTwo) {

        return numberOne * numberTwo;
    }

    /**
     * Метод выполняет деление двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - частное
     */
    static double division(double numberOne, double numberTwo) {

        return numberOne / numberTwo;
    }

    /**
     * Метод выполняет сложение двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - сумма
     */
    static double addition(double numberOne, double numberTwo) {

        return numberOne + numberTwo;
    }

    /**
     * Метод выполняет вычитание двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - разность
     */
    static double subtraction(double numberOne, double numberTwo) {

        return numberOne - numberTwo;
    }

    /**
     * Метод возводит в степень
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - число возведённое в степень
     */
    static double pow(double numberOne, double numberTwo) {
        return Math.pow(numberOne, numberTwo);
    }
}

