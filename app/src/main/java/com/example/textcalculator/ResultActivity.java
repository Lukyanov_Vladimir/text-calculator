package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    Double result, firstTerm, secondTerm;
    String mathSymbol;
    Intent intent;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        intent = getIntent();

        result = intent.getDoubleExtra("result", 0);
        firstTerm = intent.getDoubleExtra("firstTerm", 0);
        secondTerm = intent.getDoubleExtra("secondTerm", 0);
        mathSymbol = intent.getStringExtra("mathSymbol");

        textViewResult = findViewById(R.id.textViewResult);

        if (secondTerm < 0) {
            textViewResult.setText(firstTerm.toString() + mathSymbol + "(" + secondTerm.toString() + ") = " + result.toString());
        } else {
            textViewResult.setText(firstTerm.toString() + mathSymbol + secondTerm.toString() + " = " + result.toString());
        }
    }
}
